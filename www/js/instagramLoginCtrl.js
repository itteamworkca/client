appControllers.controller('instagramLoginCtrl', function ($scope, $state, $cordovaOauth, $http, localStorage, $rootScope) {

    $rootScope.igloginstart = function(){
        $scope.login();
    }

    // This function is the first activity in the controller.
    // It will initial all variable data and let the function works when page load.
    $scope.initialForm = function () {

        // $scope.isLogin is the variable for check that user is login or not.
        $scope.isLogin = false;

        // $scope.isLoading is the variable for loading progress.
        $scope.isLoading = false;

        // $scope.userInfo is the variable that store user information data.
        $scope.userInfo = {
            name: "",
            bio: "",
            website: "",
            pictureProfileUrl: "",
            socialId: "",
            post: 0,
            followers: 0,
            following: 0,
            access_token: ""
        };

        // Getting user information.
        $scope.userInfo = $scope.getUserProfile();
    };// End initialForm.

    // navigateTo is for navigate to other page
    // by using targetPage to be the destination page.
    // Parameter :
    // targetPage = destination page.
    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };// End navigateTo.

    // goToUserProfile is for navigate to instagram Profile page.
    $scope.goToUserProfile = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;
            $scope.navigateTo('app.start');
            $scope.isLoading = false;
        }
    };// End goToUserProfile.

    //getUserProfile is for get user information form localStorage by calling localStorage.get service.
    $scope.getUserProfile = function () {
        $scope.userInfo = localStorage.get("Instagram");
        if ($scope.userInfo != null) {
            $scope.isLogin = true;
        }
        ;
        return $scope.userInfo;
    };// End getUserProfile.

    // login for instagram.
    $scope.login = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;

            // Calling $cordovaOauth.instagram for login instagram.
            // Format:
            // $cordovaOauth.instagram(CLIENT_ID,[INSTAGRAM_PERMISION])
            // For CLIENT_ID is window.globalVariable.oAuth.instagram from www/js/app.js at globalVariable session.
            $cordovaOauth.instagram(window.globalVariable.oAuth.instagram, ["basic", "likes", "comments"]).then(function (result) {
                 //After call cordovaOauth.instagram it will return access_token for you to calling instagram API.
                    $scope.accessToken = result.access_token;
                    // Calling http service for getting user profile from instagram.
                    // By send parameter access_token , format.
                    $http.get("https://api.instagram.com/v1/users/self", {
                        params: {
                            access_token: result.access_token,
                            format: "json"
                        }
                    }).then(function (result) {
                        // Success retrieve data by calling http service.
                        // Store user profile information from instagram API to userInfo variable.
                        $scope.userInfo = {
                            name: result.data.data.username,
                            bio: result.data.data.bio,
                            website: result.data.data.website,
                            pictureProfileUrl: result.data.data.profile_picture,
                            socialId: result.data.data.id,
                            post: result.data.data.counts.media,
                            followers: result.data.data.counts.followed_by,
                            following: result.data.data.counts.follows,
                            access_token: $scope.accessToken
                        };
                        // Store user profile information to localStorage service.
                        localStorage.set("Instagram", $scope.userInfo);
                        // Navigate to instagram profile page.
                        $scope.navigateTo("app.start");

                        $rootScope.socialLogin = "Instagram";

                    });
                }
                , function (error) {
                    // Error retrieve data.
                    console.log(error);
                });
            $scope.isLoading = false;
        }
    };// End login.
    $scope.initialForm();
});
