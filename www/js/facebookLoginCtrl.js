appControllers.controller('facebookLoginCtrl', function ($scope, $state, $cordovaOauth, $http, localStorage, $rootScope) {

    $rootScope.fbloginstart = function(){
        $scope.login();
    }

    // This function is the first activity in the controller.
    // It will initial all variable data and let the function works when page load.
    $scope.initialForm = function () {

        // $scope.isLogin is the variable for check that user is login or not.
        $scope.isLogin = false;

        // $scope.isLoading is the variable for loading progress.
        $scope.isLoading = false;

        // $scope.userInfo is the variable that store user information data.
        $scope.userInfo = {
            name: "",
            first_name: "",
            last_name: "",
            email: "",
            birthday: "",
            link: "",
            cover: "",
            pictureProfileUrl: "",
            gender: "",
            socialId: "",
            access_token: ""
        };

        // Getting user information.
        $scope.userInfo = $scope.getUserProfile();
    }; // End initialForm.

    // navigateTo is for navigate to other page
    // by using targetPage to be the destination page.
    // Parameter :
    // targetPage = destination page.
    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };// End navigateTo.

    // goToUserProfile is for navigate to facebook Profile page.
    $scope.goToUserProfile = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;
            $scope.navigateTo('app.start');
            $scope.isLoading = false;
        }
    };// End goToUserProfile.

    //getUserProfile is for get user information form localStorage by calling localStorage.get service.
    $scope.getUserProfile = function () {
        $scope.userInfo = localStorage.get("Facebook");
        if ($scope.userInfo != null) {
            $scope.isLogin = true;
        }
        ;
        return $scope.userInfo;
    };// End getUserProfile.

    // login for facebook login
    $scope.login = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;

            // Calling $cordovaOauth.facebook for login facebook.
            // Format:
            // $cordovaOauth.facebook(APP_ID,[FACEBOOK_PERMISION])
            // For APP_ID is window.globalVariable.oAuth.facebook from www/js/app.js at globalVariable session.
            $cordovaOauth.facebook(window.globalVariable.oAuth.facebook, ["email", "public_profile", "user_friends"]).then(function (result) {
            //After call cordovaOauth.facebook it will return access_token for you to calling facebook API.

                    $scope.accessToken = result.access_token;
                    // Calling http service for getting user profile from facebook.
                    // By send parameter access_token , fields, format.
                    $http.get("https://graph.facebook.com/v2.4/me", {
                        params: {
                            access_token: result.access_token,
                            fields: "birthday,first_name,email,last_name,name,link,cover,gender,id",
                            format: "json"
                        }
                    }).then(function (result) {
                        // Success retrieve data by calling http service.
                        // Store user profile information from facebook API to userInfo variable.
                        $scope.userInfo = {
                            name: result.data.name,
                            first_name: result.data.first_name,
                            last_name: result.data.last_name,
                            email: result.data.email,
                            birthday: result.data.birthday,
                            link: result.data.link,
                            cover: result.data.cover,
                            pictureProfileUrl: "http://graph.facebook.com/" + result.data.id + "/picture?width=500&height=500",
                            gender: result.data.gender,
                            socialId: result.data.id,
                            access_token: $scope.accessToken
                        };
                        // Store user profile information to localStorage service.
                        localStorage.set("Facebook", $scope.userInfo);
                        // Navigate to facebook profile page.
                        $scope.navigateTo("app.start");
                        $rootScope.socialLogin = "Facebook";
                    });
                }
                , function (error) {
                    // Error retrieve data.
                    console.log(error);
                });
            $scope.isLoading = false;
        }
    };// End login.

    $scope.initialForm();

});// End of facebook login controller.
