appControllers.controller('foursquareLoginCtrl', function ($scope, $filter, $state, $cordovaOauth, $http, localStorage, $rootScope) {

    $rootScope.fsloginstart = function(){
        $scope.login();
    }

    // This function is the first activity in the controller.
    // It will initial all variable data and let the function works when page load.
    $scope.initialForm = function () {

        // $scope.isLogin is the variable for check that user is login or not.
        $scope.isLogin = false;

        // $scope.isLoading is the variable for loading progress.
        $scope.isLoading = false;

        // $scope.userInfo is the variable that store user information data.
        $scope.userInfo = {
            name: "",
            gender: "",
            email: "",
            pictureProfileUrl: "",
            socialId: "",
            homeCity: "",
            access_token: ""
        };
        // Getting user information.
        $scope.userInfo = $scope.getUserProfile();
        // Getting current date.
        $scope.today = $filter('date')(new Date(), "yyyyMMdd");
    };// End initialForm.

    // navigateTo is for navigate to other page
    // by using targetPage to be the destination page.
    // Parameter :
    // targetPage = destination page.
    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };// End navigateTo.

    // goToUserProfile is for navigate to foursquare Profile page.
    $scope.goToUserProfile = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;
            $scope.navigateTo('app.start');
            $scope.isLoading = false;
        }
    };// End goToUserProfile.

    //getUserProfile is for get user information form localStorage by calling localStorage.get service.
    $scope.getUserProfile = function () {
        $scope.userInfo = localStorage.get("Foursquare");
        if ($scope.userInfo != null) {
            $scope.isLogin = true;
        }
        ;
        return $scope.userInfo;
    };// End getUserProfile.

    // login for foursquare login
    $scope.login = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;

            // Calling $cordovaOauth.foursquare for login foursquare.
            // Format:
            // $cordovaOauth.foursquare(CLIENT_ID,[FOURSQUARE_PERMISION])
            // For CLIENT_ID is window.globalVariable.oAuth.foursquare from www/js/app.js at globalVariable session.
            $cordovaOauth.foursquare(window.globalVariable.oAuth.foursquare).then(function (result) {
                //After call cordovaOauth.foursquare it will return access_token for you to calling foursquare API.
                    $scope.accessToken = result.access_token;
                    // Calling http service for getting user profile from foursquare.
                    // By send parameter access_token , v (v is current version date).
                    $http.get("https://api.foursquare.com/v2/users/self", {
                        params: {
                            oauth_token: result.access_token,
                            v: $scope.today
                        }
                    }).then(function (result) {
                        // Success retrieve data by calling http service.
                        // Store user profile information from foursquare API to userInfo variable.
                        $scope.userInfo = {
                            name: result.data.response.user.firstName + " " + result.data.response.user.lastName,
                            gender: result.data.response.user.gender,
                            email: result.data.response.user.contact.email,
                            pictureProfileUrl: result.data.response.user.photo.prefix + "256x256" + result.data.response.user.photo.suffix,
                            socialId: result.data.response.user.id,
                            homeCity: result.data.response.user.homeCity,
                            access_token: $scope.accessToken
                        };
                        // Store user profile information to localStorage service.
                        localStorage.set("Foursquare", $scope.userInfo);
                        // Navigate to foursquare profile page.
                        $scope.navigateTo("app.start");
                        $rootScope.socialLogin = "Foursquare";
                    });

                }
                , function (error) {
                    // Error retrieve data.
                    console.log(error);
                });
            $scope.isLoading = false;
        }
    };// End login.
    $scope.initialForm();
});// End of foursquare login controller.
