var appControllers = angular.module('starter.controllers', []); // Use for all controller of application.

appControllers

.controller('AppCtrl', function($ionicPlatform, $scope, $ionicModal, $timeout, $ionicHistory, $state, $log, $rootScope, $ionicPopup, $ionicLoading, $http, WEBSERVICE) {

    $scope.inputs = {};
    $scope.query = {};

    $scope.navigateTo = function (stateName, setroot) {
        $timeout(function () {
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: false,
                    disableBack: false
                });
            }

            if(setroot){
                $ionicHistory.nextViewOptions({
                    historyRoot: true,
                });
            }
            $state.go(stateName);
        }, ($scope.isAndroid == false ? 300 : 0));
    }

    $rootScope.$on('$ionicView.beforeEnter', function (e, data) {
        $scope.toggleLogo = !data.enableBack;
    })

    $scope.removeSearch = function(){
      $scope.query = {};
    }

    $scope.searchContacts = function () {
      var showsearch = $ionicPopup.show({
        cssClass: 'large',
        template: '<input type="text" ng-model="query.nome" placeholder="Insert contact name" />',
        title: 'Contacts search',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Search</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.query) {
                e.preventDefault();
              } else {
                return $scope.query;
              }
            }
          }
        ]
      });
    }

    $scope.addContacts = function () {
      var showaddcontacts = $ionicPopup.show({
        cssClass: 'large',
        template: '<input type="email" ng-model="inputs.add" placeholder="Insert contact email" />',
        title: 'Add new contact',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Add</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.inputs.add) {
                e.preventDefault();
              } else {
                $ionicLoading.show({template: 'Searching contact...'});

                $http.post( WEBSERVICE + '/people/addContact/' + $rootScope.user.id , { "email": $scope.inputs.add } )
                  .then(function(success) {
                      $ionicLoading.hide();
                      $rootScope.getAllContacts();
                      $ionicPopup.alert({ title: 'Contact added' });
                  }, function(error) {
                      $ionicLoading.hide();
                      $ionicPopup.alert({ title: 'Wow! It is a error!' }).then(function(){ ionic.Platform.exitApp(); });
                  });
              }
            }
          }
        ]
      });
    }
})

.controller('startCtrl', function(WEBSERVICE, $scope, $ionicModal, $timeout, $stateParams, $rootScope, localStorage, $http, $log) {
  $rootScope.$watch('socialLogin', function() {
    if (typeof $rootScope.socialLogin === "boolean") {
      return;
    }

    var account = localStorage.get($rootScope.socialLogin);
    account.socialNetwork = $rootScope.socialLogin;

    var success = function (response) {
      $scope.navigateTo('app.contact', true);
      $rootScope.user = response.data;
      localStorage.set("user", response.data);
    }

    var error = function (response) {
      localStorage.removeAll();
      $rootScope.socialLogin = false;
    }

    $http.post(WEBSERVICE + '/people', account).then(success, error);
  });
})

.controller('contactCtrl', function($ionicPlatform, $scope, $ionicModal, $timeout, $stateParams, $ionicActionSheet, $ionicPopup, $ionicLoading, $http, WEBSERVICE, $rootScope, $state) {

      $rootScope.getAllContacts = function() {
        $scope.contacts = {};

        $ionicLoading.show({template: 'Loading contacts...'});

        $http.get( WEBSERVICE + '/people/getContacts/' + $rootScope.user.id )
                  .then(function(res) {
                      $ionicLoading.hide();
                      $scope.contacts = res.data.contacts;
                  }, function(error) {
                      $ionicLoading.hide();
                      $ionicPopup.alert({ title: 'Wow! It is a error!' }).then(function(){ ionic.Platform.exitApp(); });
                  });

      };

      $scope.showContactEmail = function(text) {
         var alertPopup = $ionicPopup.alert({
           title: 'Contact email',
           template: text
         });
      };

      $scope.loadContactChat = function(participant){
          $rootScope.userLoadChat = participant;

          var participants = {
            email1: $rootScope.user.email,
            email2: participant.email
          };

          $http.post(WEBSERVICE + '/chat/getChat', participants).then(function(response) {
            var chat = response.data;

            $http.post(WEBSERVICE + '/messages/getMessages', participants).then(function(response) {
              var messages = response.data;
              $ionicLoading.show({template: 'Loading chat...'});
              $state.go('app.chat', {chat: chat, messages: messages});
            });
          });
      }

})

.controller('chatCtrl', function($scope, $ionicModal, $timeout, $stateParams, WEBSOCKET, $location, $stomp, $log, $ionicScrollDelegate, $rootScope, $ionicLoading) {

  $scope.$on('$ionicView.enter', function(e) {
    $log.debug('chat', $stateParams);

    $ionicLoading.hide();

    $scope.chat = $stateParams.chat;
    $scope.chat.messages = $stateParams.messages.messages;

    $scope.entity = {};

    $stomp.connect(WEBSOCKET).then(function (frame) {
      $stomp.subscribe('/topic/notification/' + $scope.chat.id, function (response, headers, payload) {
        $rootScope.safeApply( function(){ $scope.chat.messages.push(response); });
      });
    });

    function autoscroll(num){
        $ionicScrollDelegate.scrollBottom();
    }

    $scope.$watch('chat.messages', function() {
        autoscroll(80);
    }, true);

    $scope.$on('onRepeatLast', function(){
        autoscroll(0);
    });

    $scope.sendMessage = function() {
      if (!$scope.entity.message || !$scope.entity.message.trim()) {
        return;
      }

      var message = {
        ownerId: $rootScope.user.id,
        chatId: $scope.chat.id,
        timestamp: new Date(),
        message: $scope.entity.message.trim()
      }

      $stomp.send('/send-message/' + $scope.chat.id, message);

      $scope.entity.message = '';

    }

  });

})
