window.globalVariable = {
    oAuth: {
        facebook: "1758228401123948",
        foursquare: "TLFQV4XD1BOPSZOCCVOG2AKLIEYWA1ME4ONRFBORI1WTZ5KN"
    }
};// End Global variable


angular.module('starter', [
  'ionic',
  'ionic.service.core',
  'starter.controllers',
  'ngStomp',
  'ngStorage',
  'ngCordovaOauth',
  'mdDialog',
  'ngMaterial'
])

.constant('WEBSERVICE', 'http://192.168.0.12:8080')
.constant('WEBSOCKET', 'http://192.168.0.12:8080')

.run(function($ionicPlatform, $log, $stomp, $http, WEBSERVICE, $rootScope, localStorage, $ionicHistory, $state) {
  $log.info("chatApp Running");

  $rootScope.socialLogin = true;

  $ionicPlatform.ready(function() {
    $log.info("ionicPlatform Running");

    var push = new Ionic.Push({
      debug: true,
      onNotification: function(notification) {
        var payload = notification.payload;
        console.log(notification.message);
        console.log(notification.title);
        console.log(notification.count);
        console.log(notification.sound);
        console.log(notification.image);
        console.log(notification.additionalData);
      },
      onRegister: function(data) {
        console.log("Device token:", data.token);
        $http.post(WEBSERVICE + '/push-notification', {token: data.token}).then(function() {
          console.log('Device token saved!');
        });
      },
      pluginConfig: {
        "ios": {
          "alert": true,
          "badge": true,
          "sound": true
         },
         "android": {
            "icon": "",
            "iconColor": "#343434",
            "forceShow": true
         }
      }
    });

    push.register(function(token) {
      console.log('Registered token:', token.token);
      push.saveToken(token);
    });

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $stomp.setDebug(function (args) {
      $log.info(args);
    });

    var navigateTo = function (stateName, setroot) {
      if(setroot){
        $ionicHistory.nextViewOptions({
          historyRoot: true,
        });
      }

      $state.go(stateName);
    };

    if (localStorage.get('Facebook') || localStorage.get('Foursquare')) {
      $rootScope.user = localStorage.get('user');
      navigateTo('app.contact', true);
    } else {
      $rootScope.socialLogin = false;
    }
  });

  $rootScope.safeApply = function(fn) {
    var phase = this.$root.$$phase;
    if(phase == '$apply' || phase == '$digest') {
      if(fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/viewport.html',
    controller: 'AppCtrl'
  })

  .state('app.start', {
    url: '/start',
    views: {
      'viewport': {
        templateUrl: 'templates/start.html',
        controller: 'startCtrl'
      }
    }
  })

  .state('app.contact', {
    url: '/contact',
    cache: false,
    views: {
      'viewport': {
        templateUrl: 'templates/contact.html',
        controller: 'contactCtrl'
      }
    }
  })

  .state('app.chat', {
    url: '/chat',
    cache: false,
    views: {
      'viewport': {
        templateUrl: 'templates/chat.html',
        controller: 'chatCtrl'
      }
    },
    params: {
      chat: null,
      messages: null
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/start');
})

.filter('tsToTime', function($filter) {
    return function(input) {
        var d = new Date(input);
        return $filter('date')(d.getHours() + ":" + d.getMinutes());
    };
})

.directive('elastic', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
            }
        };
    }
])

.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function(){
            scope.$emit('onRepeatLast', element, attrs);
        }, 100);
    };
})

.factory('localStorage', function ($filter, $window) {
    return {
        get: function (key) {
            return JSON.parse($window.localStorage[key] || "null");
        },
        set: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        removeAll: function () {
            $window.localStorage.clear();
        }
    }
})

.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function(){
            scope.$emit('onRepeatLast', element, attrs);
        }, 100);
    };
})
